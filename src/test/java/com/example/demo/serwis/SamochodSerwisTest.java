package com.example.demo.serwis;

import com.example.demo.controller.SamochodController;
import com.example.demo.entity.Samochod;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.common.util.report.qual.ReportWrite;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class SamochodSerwisTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SamochodController samochodController;

    @BeforeEach
    void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(samochodController).build();
    }

    @Test
    void test() throws Exception{
        final var test = mockMvc.perform(get("/samochod")).andExpect(status().isOk()).andReturn();
        assertEquals(test.getResponse().getStatus(), 200);
    }
}
