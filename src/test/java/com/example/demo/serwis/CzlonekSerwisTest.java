package com.example.demo.serwis;

import com.example.demo.controller.CzlonekController;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CzlonekSerwisTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CzlonekController czlonekController;

    @BeforeEach
    void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(czlonekController).build();
    }

    @Test
    void test() throws Exception{
        final var mvcResult = mockMvc.perform(get("/czlonek")).andExpect(status().isOk()).andReturn();
    }

}
