package com.example.demo.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
@SpringBootTest
@AutoConfigureMockMvc
class RodzinaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RodzinaController rodzinaController;

    @BeforeEach
    void setUp(){
        mockMvc = MockMvcBuilders.standaloneSetup(rodzinaController).build();
    }

    @Test
    void test() throws Exception{
        final var mvcResult = mockMvc.perform(get("/rodzina")).andExpect(status().isOk()).andReturn();
        assertEquals(mvcResult.getResponse().getStatus(),200);
    }
}
