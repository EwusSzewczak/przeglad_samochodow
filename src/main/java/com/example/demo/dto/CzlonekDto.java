package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CzlonekDto {
    Long id;
    String imie;
    String email;
    String haslo;
    Long rodzina_czlonek_id;
}
