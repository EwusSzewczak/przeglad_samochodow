package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PracaDto {
    Long id;
    String nazwa;
    Float koszt;
    Long id_przeglad_praca;
}
