package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SamochodDto {
    private Long id;
    private String marka;
    private String model;
    private String numer_rejestracyjny;
    private Long czlonek_samochod_id;

}
