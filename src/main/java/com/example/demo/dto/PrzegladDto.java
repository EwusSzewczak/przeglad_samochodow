package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PrzegladDto {
    private Long id;
    private Date data;
    private Long samochod_przeglad;
    private Long mechanik_przeglad;
}
