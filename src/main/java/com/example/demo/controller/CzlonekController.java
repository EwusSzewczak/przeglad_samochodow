package com.example.demo.controller;

import com.example.demo.dto.CzlonekDto;
import com.example.demo.entity.Czlonek;
import com.example.demo.entity.Rodzina;
import com.example.demo.noentities.CzlonekNoEntity;
import com.example.demo.repo.CzlonekNoEntityRepo;
import com.example.demo.serwis.CzlonekSerwis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CzlonekController {
    @Autowired
    private CzlonekSerwis czlonekSerwis;

    @PostMapping("/czlonek")
    ResponseEntity <Czlonek> saveCzlonek(@RequestBody CzlonekDto czlonekDto){
        return ResponseEntity.ok(czlonekSerwis.saveCzlonek(czlonekDto));
    }

    @GetMapping("/czlonek")
    ResponseEntity <List<Czlonek>> findAll(){
        return ResponseEntity.ok(czlonekSerwis.findAll());
    }

    @GetMapping("/czlonek/{imie}/{nazwisko}")
    ResponseEntity <CzlonekNoEntity> getCzlonekByNazwiskoAndImie(@PathVariable String imie, @PathVariable String nazwisko){
        return ResponseEntity.ok(czlonekSerwis.getCzlonekByNazwiskoAndImie(nazwisko, imie));
    }

    @GetMapping("/czlonek/{nazwisko}")
    ResponseEntity <List<CzlonekNoEntity>>getCzlonekByNazwisko(@PathVariable String nazwisko){
        return ResponseEntity.ok(czlonekSerwis.getCzlonekByNazwisko(nazwisko));
    }
}
