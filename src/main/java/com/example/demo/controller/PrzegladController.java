package com.example.demo.controller;

import com.example.demo.dto.PrzegladDto;
import com.example.demo.entity.Przeglad;
import com.example.demo.serwis.PrzegladSerwis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class PrzegladController {
    @Autowired
    private PrzegladSerwis przegladSerwis;

    @PostMapping("/przeglad")
    public ResponseEntity<Przeglad>savePrzeglad(@RequestBody PrzegladDto przegladDto){
        return ResponseEntity.ok(przegladSerwis.savePrzeglad(przegladDto));
    }

    @GetMapping("/przeglad")
    public ResponseEntity<List<Przeglad>>findAll(){
        return ResponseEntity.ok(przegladSerwis.findAll());
    }
}
