package com.example.demo.controller;

import com.example.demo.dto.RodzinaDto;
import com.example.demo.entity.Rodzina;
import com.example.demo.serwis.RodzinaSerwis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RodzinaController {
    @Autowired
    private RodzinaSerwis rodzinaSerwis;

    @GetMapping("/rodzina")
    ResponseEntity <List<Rodzina>> all(){
        List <Rodzina> listRodzina = rodzinaSerwis.findAll();
        if(listRodzina == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if(listRodzina.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return ResponseEntity.ok(listRodzina);
    }

    @PostMapping("/rodzina")
    ResponseEntity <Rodzina> newRodzina(@RequestBody RodzinaDto rodzinaDto){
        return ResponseEntity.ok(rodzinaSerwis.saveRodzina(rodzinaDto));
    }


}
