package com.example.demo.controller;

import com.example.demo.dto.PracaDto;
import com.example.demo.entity.Praca;
import com.example.demo.serwis.PracaSerwis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PracaController {
    @Autowired
    private PracaSerwis pracaSerwis;

    @PostMapping("/praca")
    public ResponseEntity<Praca> savePraca(@RequestBody PracaDto pracaDto){
        return ResponseEntity.ok(pracaSerwis.savePraca(pracaDto));
    }

    @GetMapping("/praca")
    public ResponseEntity <List<Praca>> findAll(){
        return ResponseEntity.ok(pracaSerwis.findAll());
    }

}
