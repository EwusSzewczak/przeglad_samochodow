package com.example.demo.controller;

import com.example.demo.dto.MechanikDto;
import com.example.demo.entity.Mechanik;
import com.example.demo.serwis.MechanikSerwis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MechanikController {
    @Autowired
    private MechanikSerwis mechanikSerwis;

    @PostMapping("/mechanik")
    ResponseEntity<Mechanik> saveMechanik(@RequestBody MechanikDto mechanikDto){
        return ResponseEntity.ok(mechanikSerwis.saveMechanik(mechanikDto));
    }

    @GetMapping("/mechanik")
    ResponseEntity<List<Mechanik>>findAll(){
        return ResponseEntity.ok(mechanikSerwis.findAll());
    }
}
