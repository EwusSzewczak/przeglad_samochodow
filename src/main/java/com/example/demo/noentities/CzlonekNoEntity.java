package com.example.demo.noentities;

import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.Transient;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@Transient
public class CzlonekNoEntity {
    @Id
    Long id;
    String imie;
    String email;
    String nazwisko;

}
