package com.example.demo.repo;

import com.example.demo.entity.Samochod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SamochodRepo extends JpaRepository<Samochod, Long> {
    public Samochod findSamochodById(Long id);
}
