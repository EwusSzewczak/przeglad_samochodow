package com.example.demo.repo;

import com.example.demo.entity.Przeglad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrzegladRepo extends JpaRepository<Przeglad, Long> {
    public Przeglad findPrzegladById(Long id);
}
