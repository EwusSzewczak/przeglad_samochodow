package com.example.demo.repo;

import com.example.demo.entity.Mechanik;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MechanikRepo extends JpaRepository<Mechanik, Long> {
    public Mechanik findMechanikById(Long id);
}
