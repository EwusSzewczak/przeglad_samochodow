package com.example.demo.repo;

import com.example.demo.entity.Praca;
import com.example.demo.entity.Przeglad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PracaRepo extends JpaRepository<Praca, Long> {
    public Praca findPracaById(Long id);
}
