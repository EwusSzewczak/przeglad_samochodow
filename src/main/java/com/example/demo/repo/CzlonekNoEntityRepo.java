package com.example.demo.repo;

import com.example.demo.entity.Czlonek;
import com.example.demo.noentities.CzlonekNoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CzlonekNoEntityRepo extends JpaRepository<CzlonekNoEntity, Long> {
    @Query(value = "select c.id, c.imie, r.nazwisko, c.email from czlonek c inner join rodzina r on r.id = c.rodzina_czlonek_id where r.nazwisko = :nazwisko and c.imie = :imie", nativeQuery = true)
    public CzlonekNoEntity findCzlonekNoEntityByNazwiskoAndImie(@Param("nazwisko") String nazwisko, @Param("imie") String imie);

    @Query(value = "select c.id, c.imie, r.nazwisko, c.email from Czlonek c inner join Rodzina r on r.id = c.rodzina_czlonek_id where r.nazwisko = :nazwisko", nativeQuery = true)
    public List<CzlonekNoEntity> findAllByNazwisko(@Param("nazwisko") String nazwisko);
}
