package com.example.demo.repo;

import com.example.demo.entity.Czlonek;
import com.example.demo.entity.Rodzina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CzlonekRepo extends JpaRepository<Czlonek, Long> {
    public Czlonek findCzlonekById(Long id);

    /*@Query(value = "select * from czlonek c inner join rodzina r on r.id = c.rodzina_czlonek_id where r.nazwisko = :nazwisko and c.imie = :imie", nativeQuery = true)
    public Czlonek findCzlonekByNazwiskoAndImie(@Param("nazwisko") String nazwisko, @Param("imie") String imie);*/
}
