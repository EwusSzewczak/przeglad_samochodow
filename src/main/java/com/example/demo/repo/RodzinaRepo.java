package com.example.demo.repo;

import com.example.demo.entity.Rodzina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RodzinaRepo extends JpaRepository<Rodzina, Long> {
    public Rodzina findRodzinaById(Long id);


}
