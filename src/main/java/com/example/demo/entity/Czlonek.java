package com.example.demo.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Czlonek {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String imie;

    @Column(nullable = false, unique = true, length = 45)
    private String email;

    @Column(nullable = false, length = 64)
    private String haslo;

    @ManyToOne
    private Rodzina rodzina_czlonek;

    @OneToMany(mappedBy = "czlonek_samochod", cascade = CascadeType.ALL)
    private Set<Samochod> samochod;

    public Czlonek(String imie, String email,String haslo,  Rodzina rodzina_czlonek) {
        this.imie = imie;
        this.email = email;
        this.rodzina_czlonek = rodzina_czlonek;
        this.haslo = haslo;
    }

    public Czlonek() {
    }

    public Long getId() {
        return id;
    }

    public String getImie() {
        return imie;
    }

    public String getEmail() {
        return email;
    }

    public Rodzina getRodzina_czlonek() {
        return rodzina_czlonek;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRodzina_czlonek(Rodzina rodzina_czlonek) {
        this.rodzina_czlonek = rodzina_czlonek;
    }

    public String getPassword() {
        return haslo;
    }

    public void setPassword(String haslo) {
        this.haslo = haslo;
    }
}
