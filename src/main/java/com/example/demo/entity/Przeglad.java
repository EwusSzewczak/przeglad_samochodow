package com.example.demo.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Przeglad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date data;
    @ManyToOne()
    private Samochod samochod_przeglad;
    @OneToMany(mappedBy = "przeglad_praca", cascade =CascadeType.ALL)
    private Set<Praca> praca;
    @ManyToOne()
    private Mechanik mechanik_przeglad;
    public Przeglad() {
    }

    public Date getData() {
        return data;
    }

    public Samochod getSamochod_przeglad() {
        return samochod_przeglad;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setSamochod_przeglad(Samochod samochod_przeglad) {
        this.samochod_przeglad = samochod_przeglad;
    }

    /*public Set<Praca> getPraca() {
        return praca;
    }*/

    public Mechanik getMechanik_przeglad() {
        return mechanik_przeglad;
    }

    public void setMechanik_przeglad(Mechanik mechanik_przeglad) {
        this.mechanik_przeglad = mechanik_przeglad;
    }

}
