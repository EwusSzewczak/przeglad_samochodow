package com.example.demo.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Samochod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String marka;
    private String model;
    private String numer_rejestracyjny;
    @ManyToOne
    private Czlonek czlonek_samochod;

    @OneToMany(mappedBy = "samochod_przeglad", cascade = CascadeType.ALL)
    Set<Przeglad> przeglad;

    public Samochod() {
    }

    public Samochod(Long id, String marka, String model, String numer_rejestracyjny, Czlonek czlonek_samochod) {
        this.id = id;
        this.marka = marka;
        this.model = model;
        this.numer_rejestracyjny = numer_rejestracyjny;
        this.czlonek_samochod = czlonek_samochod;
    }

    public Long getId() {
        return id;
    }

    public String getMarka() {
        return marka;
    }

    public String getModel() {
        return model;
    }

    public String getNumer_rejestracyjny() {
        return numer_rejestracyjny;
    }

    public Czlonek getCzlonek_samochod() {
        return czlonek_samochod;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setNumer_rejestracyjny(String numer_rejestracyjny) {
        this.numer_rejestracyjny = numer_rejestracyjny;
    }

    public void setCzlonek_samochod(Czlonek czlonek_samochod) {
        this.czlonek_samochod = czlonek_samochod;
    }
}
