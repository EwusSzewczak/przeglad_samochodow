package com.example.demo.entity;

import javax.persistence.*;

@Entity
public class Praca {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nazwa;
    private Float koszt;
    @ManyToOne()
    private Przeglad przeglad_praca;

    public Praca() {
    }

  /*  public Praca(String nazwa, Float koszt, Przeglad przeglad_praca) {
        this.nazwa = nazwa;
        this.koszt = koszt;
        this.przeglad_praca = przeglad_praca;
    }*/

    public String getNazwa() {
        return nazwa;
    }

    public Float getKoszt() {
        return koszt;
    }

    public Przeglad getPrzeglad_praca() {
        return przeglad_praca;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setKoszt(Float koszt) {
        this.koszt = koszt;
    }

    public void setPrzeglad_praca(Przeglad przeglad_praca) {
        this.przeglad_praca = przeglad_praca;
    }
}
