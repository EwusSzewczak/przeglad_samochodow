package com.example.demo.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Rodzina {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nazwisko;

    public Rodzina(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Rodzina() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    @OneToMany(mappedBy = "rodzina_czlonek", cascade = CascadeType.ALL)
    private Set<Czlonek> czlonek;


    /*public void setCzlonek(Set<Czlonek> czlonek) {
        this.czlonek = czlonek;
    }*/

    /*public Set<Czlonek> getCzlonek() {
        return czlonek;
    }*/
}
