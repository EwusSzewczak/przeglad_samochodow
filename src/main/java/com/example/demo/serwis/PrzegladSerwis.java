package com.example.demo.serwis;

import com.example.demo.dto.PrzegladDto;
import com.example.demo.entity.Przeglad;
import com.example.demo.repo.MechanikRepo;
import com.example.demo.repo.PracaRepo;
import com.example.demo.repo.PrzegladRepo;
import com.example.demo.repo.SamochodRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class PrzegladSerwis {
    @Autowired
    private PrzegladRepo przegladRepo;

    @Autowired
    private SamochodRepo samochodRepo;

    @Autowired
    private MechanikRepo mechanikRepo;

    public Przeglad savePrzeglad(PrzegladDto przegladDto){
        Przeglad przeglad = new Przeglad();
        przeglad.setData(przegladDto.getData());
        przeglad.setSamochod_przeglad(samochodRepo.findSamochodById(przegladDto.getSamochod_przeglad()));
        przeglad.setMechanik_przeglad(mechanikRepo.findMechanikById(przegladDto.getMechanik_przeglad()));
        przegladRepo.save(przeglad);
        return przeglad;
    }
    public List<Przeglad> findAll(){
        return przegladRepo.findAll();
    }
}
