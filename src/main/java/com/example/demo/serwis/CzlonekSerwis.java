package com.example.demo.serwis;

import com.example.demo.dto.CzlonekDto;
import com.example.demo.entity.Czlonek;
import com.example.demo.entity.Rodzina;
import com.example.demo.noentities.CzlonekNoEntity;
import com.example.demo.repo.CzlonekNoEntityRepo;
import com.example.demo.repo.CzlonekRepo;
import com.example.demo.repo.RodzinaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CzlonekSerwis {
    @Autowired
    private CzlonekRepo czlonekRepo;

    @Autowired
    private RodzinaRepo rodzinaRepo;

    @Autowired
    private CzlonekNoEntityRepo czlonekNoEntityRepo;

    public Czlonek saveCzlonek(CzlonekDto czlonekDto){
        Czlonek czlonek = new Czlonek();
        czlonek.setImie(czlonekDto.getImie());
        czlonek.setEmail(czlonekDto.getEmail());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(czlonekDto.getHaslo());
        czlonek.setPassword(encodedPassword);

        /*rodzinaRepo.findRodzinaById(czlonekDto.getRodzina_czlonek_id());*/
        czlonek.setRodzina_czlonek(rodzinaRepo.findRodzinaById(czlonekDto.getRodzina_czlonek_id()));
        czlonekRepo.save(czlonek);
        return czlonek;
    }
    public List<Czlonek> findAll(){
        return czlonekRepo.findAll();
    }

    public CzlonekNoEntity getCzlonekByNazwiskoAndImie(String nazwisko, String imie){
        return czlonekNoEntityRepo.findCzlonekNoEntityByNazwiskoAndImie(nazwisko, imie);
    }

    public List<CzlonekNoEntity> getCzlonekByNazwisko(String nazwisko){
        return czlonekNoEntityRepo.findAllByNazwisko(nazwisko);
    }

}
