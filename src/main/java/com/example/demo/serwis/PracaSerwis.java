package com.example.demo.serwis;

import com.example.demo.dto.PracaDto;
import com.example.demo.entity.Praca;
import com.example.demo.entity.Przeglad;
import com.example.demo.repo.PracaRepo;
import com.example.demo.repo.PrzegladRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PracaSerwis {
    @Autowired
    private PracaRepo pracaRepo;

    @Autowired
    private PrzegladRepo przegladRepo;

    public Praca savePraca(PracaDto pracaDto){
        Praca praca = new Praca();
        praca.setNazwa(pracaDto.getNazwa());
        praca.setKoszt(pracaDto.getKoszt());
        praca.setPrzeglad_praca(przegladRepo.findPrzegladById(pracaDto.getId_przeglad_praca()));
        pracaRepo.save(praca);
        return praca;
    }
    public List<Praca> findAll(){
        return pracaRepo.findAll();
    }
}
