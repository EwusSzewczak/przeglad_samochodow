package com.example.demo.serwis;

import com.example.demo.dto.RodzinaDto;
import com.example.demo.entity.Rodzina;
import com.example.demo.repo.RodzinaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RodzinaSerwis {
    @Autowired
    private RodzinaRepo rodzinaRepo;

    public Rodzina saveRodzina(RodzinaDto rodzinaDto){
        Rodzina rodzina=new Rodzina();
        rodzina.setNazwisko(rodzinaDto.getNazwisko());
        rodzinaRepo.save(rodzina);
        return  rodzina;
    }

    public List<Rodzina> findAll(){
        return rodzinaRepo.findAll();
    }

}
