package com.example.demo.serwis;

import com.example.demo.dto.MechanikDto;
import com.example.demo.entity.Mechanik;
import com.example.demo.repo.MechanikRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MechanikSerwis {
    @Autowired
    private MechanikRepo mechanikRepo;

    public Mechanik saveMechanik(MechanikDto mechanikDto){
        Mechanik mechanik = new Mechanik();
        mechanik.setImie(mechanikDto.getImie());
        mechanik.setNazwisko(mechanikDto.getNazwisko());
        mechanik.setAdres(mechanikDto.getAdres());
        mechanik.setTelefon(mechanikDto.getTelefon());
        mechanikRepo.save(mechanik);
        return mechanik;
    }

    public List<Mechanik> findAll(){
        return mechanikRepo.findAll();
    }
}
