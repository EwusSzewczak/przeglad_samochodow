package com.example.demo.serwis;

import com.example.demo.dto.SamochodDto;
import com.example.demo.entity.Czlonek;
import com.example.demo.entity.Samochod;
import com.example.demo.repo.CzlonekRepo;
import com.example.demo.repo.SamochodRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SamochodSerwis {
    @Autowired
    private SamochodRepo samochodRepo;

    @Autowired
    private CzlonekRepo czlonekRepo;

    public Samochod saveSamochod(SamochodDto samochodDto){
        Samochod samochod = new Samochod();
        samochod.setMarka(samochodDto.getMarka());
        samochod.setModel(samochodDto.getModel());
        samochod.setNumer_rejestracyjny(samochodDto.getNumer_rejestracyjny());
        samochod.setCzlonek_samochod(czlonekRepo.findCzlonekById(samochodDto.getCzlonek_samochod_id()));
        samochodRepo.save(samochod);
        return samochod;
    }

    public List<Samochod> findAll(){
     return samochodRepo.findAll();
    }
}
